$('document').ready(function(){
    $('.slbutt').on('click', function(e){      
        e.preventDefault();
        var href = $(this).attr('href');
        getContent(href, true);
    });
});

window.addEventListener("popstate", function(e) {
    getContent(location.pathname, false);
    
});

function getContent(url, addEntry) {
    $.get(url).done(function(data) {
        if(addEntry == true) {
            history.pushState(null, null, url);
        }
    });
}
$(document).ready(function(){
    $(".slbutt").on("click", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});