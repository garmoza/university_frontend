import React from 'react';
import * as types from './actionsTypes';
import CloseForm from '../components/CloseForm';
import Empty from '../components/Empty';
import ActiveSendButton from '../components/ActiveSendButton'

const initialState = {
    name: '',
    email: '',
    comment: '',
    thatState: <CloseForm />,
    loadState: <Empty />,
    sendButtonState: <ActiveSendButton />
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
  
        case types.CHANGE_NAME:
            return {
                ...state,
                name: action.payload,
            };

        case types.CHANGE_EMAIL:
            return {
                ...state,
                email: action.payload
            };

        case types.CHANGE_COMMENT:
            return {
                ...state,
                comment: action.payload
            };

        case types.FORM_OPEN:
            return {
                ...state,
                thatState: action.payload
            };

        case types.FORM_CLOSE:
            return {
                ...state,
                thatState: action.payload
            };

        case types.QUERY_EXECUTION:
            return {
                ...state,
                loadState: action.payload
            };

        case types.SUCCESS_LOAD:
            return {
                ...state,
                loadState: action.payload
            };

        case types.REQUEST_ERROR:
            return {
                ...state,
                loadState: action.payload
            };

        case types.EMPRY_LOAD:
            return {
                ...state,
                loadState: action.payload
            };

        case types.ACTIVE_BUTTON:
            return {
                ...state,
                sendButtonState: action.payload
            };

        case types.LOCKED_BUTTON:
            return {
                ...state,
                sendButtonState: action.payload
            };
  
        default:
            return state;
    }
}

/*
export default function reducer(state = [], action) {
    switch (action.type) {
  
        case types.LOCKED_BUTTON:
            return [
                ...state,
                action.type
            ];

        default:
            return state;
    }
}
*/
  