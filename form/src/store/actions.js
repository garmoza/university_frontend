import React from 'react';
import * as types from './actionsTypes';
import OpenForm from '../components/OpenForm';
import CloseForm from '../components/CloseForm';
import Load from '../components/Load';
import SuccessLoad from '../components/SuccessLoad';
import Empty from '../components/Empty'
import ErrorLoad from '../components/ErrorLoad'
import ActiveSendButton from '../components/ActiveSendButton'
import DisableSendButton from '../components/DisableSendButton'

export const changeName = (newName)=>{
  return{
      type: types.CHANGE_NAME,
      payload: newName
  };
};

export const changeEmail = (newEmail)=>{
  return{
      type: types.CHANGE_EMAIL,
      payload: newEmail
  };
};

export const changeComment = (newComment)=>{
  return{
      type: types.CHANGE_COMMENT,
      payload: newComment
  };
};

export function activeButton() {
  return {
    type: types.ACTIVE_BUTTON,
    payload: <ActiveSendButton />
  };
}

export function lockedButton() {
    return {
      type: types.LOCKED_BUTTON,
      payload: <DisableSendButton />
    };
}

export function queryExecution() {
    return {
      type: types.QUERY_EXECUTION,
      payload: <Load />
    };
}

export function successLoad() {
  return {
    type: types.SUCCESS_LOAD,
    payload: <SuccessLoad />
  };
}

export function requestError() {
    return {
      type: types.REQUEST_ERROR,
      payload: <ErrorLoad />
    };
}

export function emptyLoad() {
  return {
    type: types.EMPRY_LOAD,
    payload: <Empty />
  };
}

export function formOpen() {
    return {
      type: types.FORM_OPEN,
      payload: <OpenForm />
    };
}
export function formClose() {
    return {
      type: types.FORM_CLOSE,
      payload: <CloseForm />
    };
}