import React from 'react';
import '../App.css';

import {connect} from 'react-redux'
import * as actions from '../store/actions'

import {buttonType} from './cstyle'

function getLocalSotre(dispatch) {
    if (localStorage.getItem("name")) {
        dispatch(actions.changeName(localStorage.getItem("name")));
    }
    if (localStorage.getItem("email")) {
        dispatch(actions.changeEmail(localStorage.getItem("email")));
    }
    if (localStorage.getItem("comment")) {
        dispatch(actions.changeComment(localStorage.getItem("comment")));
    }
}

class CloseForm extends React.Component {
    render() {
        const dispatch = this.props.dispatch;
        return (
            <div>
                <button
                    className="btn butt3 open"
                    type="button"
                    href="open"
                    style={buttonType}
                    value="Open"
                    id="button_type"
                    onClick={() => {
                        dispatch(actions.formOpen());
                        document.getElementById("form_link").style.height = '420px';
                        getLocalSotre(dispatch);
                    }}
                >
                <span id="button_text">Open</span>
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        name: state.name,
        email: state.email,
        comment: state.comment,
        thatState: state.thatState,
        loadState: state.loadState
    };
};

const WrappedCloseForm = connect(mapStateToProps)(CloseForm);

export default WrappedCloseForm;
