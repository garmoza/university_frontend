import React from 'react';

import Dog from '../gif/dog.gif';
import {dogStyle} from './cstyle'

export default class SuccessLoad extends React.Component {
    render() {

        return (
            <div><img src={Dog} alt='dog' style={dogStyle} /></div>
        );
    }
}