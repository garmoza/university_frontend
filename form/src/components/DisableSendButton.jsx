import React from 'react';

import {buttonType} from './cstyle'

export default class DisableSendButton extends React.Component {
    render() {
        return (
            <button className="btn butt3" type="submit" style={buttonType}value="Send" id="button_type" disabled>
                <span id="button_text">Send</span>
            </button>
        );
    }
}