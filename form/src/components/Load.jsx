import React from 'react';

import Cat from '../gif/cat.gif';
import {catStyle} from './cstyle'

export default class Load extends React.Component {
    render() {
        return (
            <div><img src={Cat} alt='cat' style={catStyle} /></div>
        );
    }
}