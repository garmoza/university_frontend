import React from 'react';

import {buttonType} from './cstyle'

export default class ActiveSendButton extends React.Component {
    render() {
        return (
            <button className="btn butt3" type="submit" style={buttonType}value="Send" id="button_type">
                <span id="button_text">Send</span>
            </button>
        );
    }
}