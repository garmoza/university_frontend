export const divStyle={
    display: 'flex',
    flexDirection: 'column',
    width: '220px',
    height:'190px',
    
  };
export const area={
    borderRadius:'20px',
    marginBottom:'10px',
    height:'110px'
};
export const inputs={
    height:'30px',
    borderRadius:'20px',
    marginBottom:'10px',
};
export const buttonType = {
    width:'220px',
    height:'33px',
    backgroundColor:'red',
    color:'white',
    fontSize:'20px',
    border:'none',
    borderRadius:'20px',
};

export const dogStyle={
    position: 'absolute',
    width: '150px',
    bottom: '80px',
    left: '40px'
};

export const catStyle={
    position: 'absolute',
    width: '150px',
    bottom: '80px',
    left: '40px'
};

export const GhostStyle={
    position: 'absolute',
    width: '150px',
    bottom: '80px',
    left: '40px'
};
