import React from 'react';
import '../App.css';

import {connect} from 'react-redux'
import * as actions from '../store/actions'

import {divStyle, area, inputs, buttonType} from './cstyle'

function get_promise(dispatch, props) {
    return new Promise((resolve, reject) => {
        fetch('https://formcarry.com/s/79D4V9NBj_0', {//'https://formcarry.com/s/eDA5IVwfbLO'
            method: 'POST',
            headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            body: JSON.stringify({name: props.name, email: props.email, comment: props.comment})
        })
        .then(response => {
            console.log(response);
            localStorage.clear();
            dispatch(actions.successLoad());
            dispatch(actions.changeName(""));
            dispatch(actions.changeEmail(""));
            dispatch(actions.changeComment("")); 
            resolve();
        })
        .catch(error => {
            console.log(error); 
            dispatch(actions.requestError());
            reject();
        })
    });
}

class Form extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        const dispatch = this.props.dispatch;
        event.preventDefault();
        dispatch(actions.queryExecution());
        dispatch(actions.lockedButton());
        await get_promise(dispatch, this.props);
        dispatch(actions.activeButton());
        setTimeout(() => dispatch(actions.emptyLoad()),1500);
    }

    render() {
        const dispatch = this.props.dispatch;

        return (
        <form onSubmit={this.handleSubmit}>
            <div className = "fields" id = "fields" style = {divStyle}>
                <input
                    name="name"
                    type="text"
                    style={inputs}
                    placeholder="Name"
                    value={this.props.name}
                    onChange={(event) => {dispatch(actions.changeName(event.target.value)); localStorage.setItem("name", event.target.value);}}
                    autoFocus required
                />
                <input
                    name="email"
                    type="email"
                    style={inputs}
                    placeholder="E-mail"
                    value={this.props.email}
                    onChange={(event) => {dispatch(actions.changeEmail(event.target.value)); localStorage.setItem("email", event.target.value);}}
                    required
                />
                <textarea
                    name="comment"
                    style={area}
                    placeholder="Comment" 
                    rows="5" cols="60"
                    value={this.props.comment}
                    onChange={(event) => {dispatch(actions.changeComment(event.target.value)); localStorage.setItem("comment", event.target.value);}}
                    required
                />
            </div>
            {this.props.sendButtonState}
            <button className="btn butt3" type="button" style={buttonType} value="Close" id="button_type" onClick={() => {
                dispatch(actions.formClose());
                document.getElementById("form_link").style.height = '170px';
            }}>
                <span id="button_text">Close</span>
            </button>
        </form>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        name: state.name,
        email: state.email,
        comment: state.comment,
        thatState: state.thatState,
        loadState: state.loadState,
        sendButtonState: state.sendButtonState
    };
};

const WrappedForm = connect(mapStateToProps)(Form);

export default WrappedForm;
