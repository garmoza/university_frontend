import React from 'react';

import Ghost from '../gif/ghost.png';
import {GhostStyle} from './cstyle'

export default class ErrorLoad extends React.Component {
    render() {
        return (
            <div><img src={Ghost} alt='ghost' style={GhostStyle} /></div>
        );
    }
}