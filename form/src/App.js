import React from 'react';
import './App.css';

import {connect} from 'react-redux'

class App extends React.Component {
    render() {
        return (
            <div>
                {this.props.loadState}
                {this.props.thatState}  
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        name: state.name,
        email: state.email,
        comment: state.comment,
        thatState: state.thatState,
        loadState: state.loadState,
        sendButtonState: state.sendButtonState
    };
};

const WrappedApp = connect(mapStateToProps)(App);

export default WrappedApp;
