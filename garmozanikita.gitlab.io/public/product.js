function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = [10, 50, 100];
    let pricesd = [63];
    let kolvo = document.getElementById("kolvo").value;

    //Устанавливаем цену для 1 элемента списка.
    if (select.value === "1") {
        price += 63;
    }

    // Скрываем или показываем радиокнопки.
    let radioDiv = document.getElementById("radios");
    if (select.value === "2") {
        radioDiv.style.display = "block";
        //Смотрим какой радио-батон) выбран и добавляем его стоимость к общей.
        let radios = document.querySelectorAll("#radios input");
        radios.forEach(function (radio) {
            if (radio.checked) {
                let propPrice = prices[radio.value];
                if (propPrice !== undefined) {
                    price += propPrice * 63;
                }
            }
        });
    } else {
        radioDiv.style.display = "none";
    }
    // Скрываем или показываем чекбоксы.
    let checkDiv = document.getElementById("checkboxes");
    if (select.value === "3") {
        checkDiv.style.display = "block";
        price += 133;
        // Смотрим выбран ли чекбокс.
        let checkboxes = document.querySelectorAll("#checkboxes input");
        checkboxes.forEach(function (checkbox) {
            if (checkbox.checked) {
                let propPrice = pricesd[checkbox.name];
                if (propPrice !== undefined) {
                    price += 63;
                }
            }
        });
    } else {
        checkDiv.style.display = "none";
    }
    //обновление цены
    let prodPrice = document.getElementById("prodPrice");
    if (kolvo >= 0) {
        prodPrice.innerHTML = price * kolvo + " ₽";
    } else {
        prodPrice.innerHTML = "NaN ₽";
    }
}

window.addEventListener("DOMContentLoaded", function () {
    let profit = document.getElementById("profit");
    profit.addEventListener("change", function () {
        updatePrice();
    });
    profit.addEventListener("input", function () {
        updatePrice();
    });
    updatePrice();
});